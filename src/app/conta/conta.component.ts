import { Component, OnInit, NgZone } from '@angular/core';
import { Oferta } from '../shared/Oferta.model';
import { Cliente } from '../shared/cliente.model';
import { OfertasService } from '../ofertas.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { ParamsService } from '../services/params.service';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-conta',
  templateUrl: './conta.component.html',
  styleUrls: ['./conta.component.css']
})
export class ContaComponent implements OnInit {

  public ofertas: Oferta[]
  currentUser: Cliente;
  returnUrl: string;
  private produtosCadastrados: boolean = false;
  private produtosAdmin: Array<any> = []
  public dadosUser: Array<any> = []
  public carrinhoClient: Array<any> = []
  public filterPedidos: Array<any> = []
  public userData: string
  public userName: string
  public userAdmin: boolean
  loading: boolean = false

  constructor(
    private ofertasService: OfertasService,
    private router: Router,
    private authenticationService: AuthService,
    private paramService: ParamsService,
    private zone: NgZone,
    private fireAuth: AngularFireAuth,
  ) {
    this.authenticationService.currentUser.subscribe((x) => {
      console.log(x)
      this.currentUser = x
    });
  }

  ngOnInit() {
    this.dataUser()
  }

  public async dataUser() {

    console.log(this.fireAuth.auth.currentUser)

    if (this.fireAuth.auth.currentUser) {
      this.zone.run(async () => {

        await this.authenticationService.getUser(this.fireAuth.auth.currentUser.email).then((dados) => {
          console.log('DADOS: ', dados)

          this.dadosUser = dados

          this.paramService.setUser(this.dadosUser)

          for (let index = 0; index < this.dadosUser.length; index++) {
            const element = this.dadosUser[index];
            this.userData = element.email
            this.userAdmin = element.isAdmin
            this.userName = element.nome
          }

          console.log('EMAIL USUÁRIO: ', this.userData)


          if (this.userAdmin === true) {
            this.ofertasService.getCarrinhosPorAdmin().subscribe((data) => {
              console.log('DADOS DO CARRINHO GERAL:', data)
              this.produtosAdmin = data
            })
          } else {
            this.ofertasService.getCarrinhoPorEmail(this.userData).subscribe((dados) => {

              if (dados.status === 404) {
                console.log('ENTROU AQ')
              } else {

                console.log('PEDIDOS DOS CARRINHOS: ', dados)

                this.filterPedidos = dados

                const filtered = this.filterPedidos.filter(data => data.email_cliente === this.userData)

                console.log(filtered)

                if (filtered.length) {
                  this.carrinhoClient = dados
                  this.paramService.setCarrinhoCliente(this.carrinhoClient)
                  this.produtosCadastrados = true
                }

              }

              console.log(this.carrinhoClient)

            }, (error) => {
              console.log(error)
              this.produtosCadastrados = false
            })
          }
          this.loading = true
        })

      })
    }
  }

  addProduto(event) {
    console.log(event)
    this.router.navigate(['/add-produto'])
  }

}
