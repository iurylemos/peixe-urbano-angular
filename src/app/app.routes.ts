import { Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { RestaurantesComponent } from './restaurantes/restaurantes.component';
import { DiversaoComponent } from './diversao/diversao.component';
import { OfertaComponent } from './oferta/oferta.component';
import { ComoUsarComponent } from './oferta/como-usar/como-usar.component';
import { OndeFicaComponent } from './oferta/onde-fica/onde-fica.component';
import { OrdemCompraComponent } from './ordem-compra/ordem-compra.component';
import { AuthGuard } from './services/auth.guard';
import { ContaComponent } from './conta/conta.component';
import { ProductDataComponent } from './conta/product-data/product-data.component';
import { OrderDetailsComponent } from './conta/order-details/order-details.component';

//Responsável por ter um mapa associado Paths 
//Passei o path de ofertas para a HOME
//Pois se a oferta não for parâmetrizada ela não é válida
//a componentes específicos


//Lá na rota children das ofertas
//A primeira eu coloquei como vázia, pois na apresentação 
//Pois como ela vai ser a raiz, vou apresentar um específico
//Igual eu fiz com a HomeComponente, aapresnetnado a Oferta
export const ROUTES: Routes = [
  { path: '', component: HomeComponent },
  { path: 'restaurantes', component: RestaurantesComponent },
  { path: 'diversao', component: DiversaoComponent },
  { path: 'oferta', component: HomeComponent },
  { path: 'oferta/:id_oferta', component: OfertaComponent, 
    children: [
      { path: '', component: ComoUsarComponent },
      { path: 'como-usar', component: ComoUsarComponent },
      { path: 'onde-fica', component: OndeFicaComponent }
    ] 
  },
  { path: 'ordem-compra', component: OrdemCompraComponent, canActivate: [AuthGuard] },
  { path: 'conta', component: ContaComponent, canActivate: [AuthGuard] },
  { path: 'add-produto', component: ProductDataComponent, canActivate: [AuthGuard] },
  { path: 'data-product/:id', component: OrderDetailsComponent, canActivate: [AuthGuard] },
  { path: '**', redirectTo: '' }


  //{ path: 'path/:routeParam', component: MyComponent },
  //{ path: 'staticPath', component: ... },
  //{ path: '**', component: ... },
  //{ path: 'oldPath', redirectTo: '/staticPath' },
  //{ path: ..., component: ..., data: { message: 'Custom' }
];
