import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { OfertasService } from 'src/app/ofertas.service';
import { Oferta } from 'src/app/shared/Oferta.model';
//Como estamos trabalhando com rotas

@Component({
  selector: 'app-como-usar',
  templateUrl: './como-usar.component.html',
  styleUrls: ['./como-usar.component.css'],
  providers: [ OfertasService ]
})
export class ComoUsarComponent implements OnInit {

  public comoUsar: string = ''

  constructor(
    private route: ActivatedRoute,
    private ofertasService: OfertasService
  ) { }

  ngOnInit() {

    this.route.parent.params.subscribe((parametros: Params) => {
      this.ofertasService.getComoUsarOfertaPorId(parametros.id)
      .then((resposta: any) => {
        console.log('COMPONENTE COMO USAR',resposta[0].descricao)
        this.comoUsar = resposta[0].descricao
      })
    })

    //Agora conseguimos acessar os parâmetros da rota.
    //Nesse caso aqui estou recuperando um parâmetro
    //do sistema de roteamento em questão
    //Que é o router-outlet filho do ofertas
    // this.route.snapshot.params['']
    //Mas não é isso que eu quero
    //Eu quero essar as rotas do parent.
    //E assim eu consigo acessar os parâmetros
    //Do sistema de roteamento Root, da aplicação
    //E assim por mais que haja uma modificação na hora
    //De escolher qual a oferta deseja
    //Eu vou ter condições de capturar essas informações
    //Dentro do componente de rotas filhas
    //Que são esses componentes aqui
    console.log('ID da rota Pai: ', this.route.parent.snapshot.params['id_oferta'])
    //Chamando o metodo que contem a rota no Serviço
    //Passando no parâmetro o id recuperado da rota PAI.
    //Ou seja na url vai ficar localhost:4200/ofertas/1/como-usar
    //E assim que ele entrar dentro do componente, oferta
    //Eu tenho lá em baixo o como usar com a descricao sobre essa oferta
    
  }

}
