import { Component, OnInit } from '@angular/core';
import { OrdemCompraService } from '../ordem-compra.service';
import { Pedido } from '../shared/pedido.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CarrinhoService } from '../carrinho.service';
import { ItemCarrinho } from '../shared/item-carrinho.model';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from '../auth.service';
import { User } from '../interfaces/user';

@Component({
  selector: 'app-ordem-compra',
  templateUrl: './ordem-compra.component.html',
  styleUrls: ['./ordem-compra.component.css'],
  providers: [ OrdemCompraService ]
})
export class OrdemCompraComponent implements OnInit {

  public emailCliente: string
  public dadosUser = new Array<User>();
  public idPedidoCompra: number
  //Item carrinho é um modelo para representar cada um dos itens
  //inseridos no atributos itens da nossa classe.
  //E vou utilizar ele no Oninit
  public itensCarrinho: ItemCarrinho[] = []
  public status: string = 'Aberto'
  public totalCarrinho : number

  //Esse formGroup vai ser conectado ao formulário
  //formGroup corresponde a um formulário do template
  //Já o formControl corresponde a elementos do formulário
  //Campos de interação do usuário, que fazem parte do formulário
  //Como é o caso do complemento, número ou endereço.
  public formulario: FormGroup = new FormGroup({
    'endereco': new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(120)]),
    'numero': new FormControl(null, [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
    'complemento': new FormControl(null),
    'formaPagamento': new FormControl(null, [Validators.required])
  })

  constructor(
    private ordemCompraService: OrdemCompraService,
    public carrinhoService: CarrinhoService,
    private fireAuth: AngularFireAuth,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    //Quando o componente for contruido vou recuperar os itens do carrinho
    //E associar esses itens ao itens do carrinho
    //A principal diferença para antes é que agora consigo recuperar os 
    //dados do serviço e associamos também ao atributo da classe
    //Isso significa que consigo fazer o databinding no template
    this.itensCarrinho = this.carrinhoService.exibirItens()
    console.log(this.itensCarrinho)

    //Itens associados ao item do serviço
    // console.log('Array de itens carrinho: ',this.carrinhoService.exibirItens())

    
  }

  public confirmarCompra(): void {
    //Status me retorna, valid ou invalid
    // console.log(this.formulario.status)
    if(this.formulario.status === "INVALID") {
      // console.log('Formulário está inválido')
      this.formulario.get('endereco').markAsTouched()
      this.formulario.get('numero').markAsTouched()
      this.formulario.get('complemento').markAsTouched()
      this.formulario.get('formaPagamento').markAsTouched()
    }else {
        //Caso contrário eu monto o pedido e envio para
        //OrdemCompraService
        if(this.carrinhoService.exibirItens().length === 0) {
          alert('Você não selecionou nenhum item no carrinho')
        }else {
          //Tempo aqui no pedido os dados do formulário
          //Mas não temos os itens do carrinhoService
          //Através do carrinho
          //Na prática nós precisamos pegar esses itens
          //E somar esses itens ao nosso pedido

          //Para fazer com que o pedido tenha mais um atributo
          //Que seja um array de itens de carrinho
          //Vou chamar o carrinhoService trazendo o array
          this.itensCarrinho.forEach(element => {
           this.idPedidoCompra = element.id_pedido
          });

          this.authService.getUsers().subscribe(data => {
            // this.dadosUser = data;
            this.dadosUser = data.filter((data) => data.email === this.fireAuth.auth.currentUser.email)
            console.log('DADOS DO USUÁRIO: ', this.dadosUser)
            for (let index = 0; index < this.dadosUser.length; index++) {
              const element = this.dadosUser[index];
    
              this.emailCliente = element.email
            }
            console.log('EMAIL CLIENTE:', this.emailCliente)
  
            let pedido: Pedido = new Pedido(
              this.status,
              this.emailCliente,
              this.formulario.value.endereco,
              this.formulario.value.numero,
              this.formulario.value.complemento,
              this.formulario.value.formaPagamento,
              this.carrinhoService.totalCarrinhoCompras(),
              new Date().toLocaleString(),
              this.carrinhoService.exibirItens(),
            )
              
             this.totalCarrinho = this.carrinhoService.totalCarrinhoCompras()
  
             console.log(this.totalCarrinho)
    
            this.ordemCompraService.efetivarCompra(pedido)
            .subscribe((idPedido: number) => {
              this.idPedidoCompra = idPedido
              // console.log('Imprimindo o id do pedido' +this.idPedidoCompra)
              //Além de recuperar o id do pedido
              //executar a limpeza
              this.carrinhoService.limparCarrinho()
            },(error)=>{
              console.log(error)
            })
          })
      }

    }
  }

  public adicionar(item: ItemCarrinho) : void {
    //Ele vai fazer uma ponte entre o template o serviço
    this.carrinhoService.adicionarQuantidade(item)
  }

  public diminuir(item: ItemCarrinho): void {
    this.carrinhoService.diminuirItem(item)
  }
}
