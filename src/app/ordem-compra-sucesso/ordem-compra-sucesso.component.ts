import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ordem-compra-sucesso',
  templateUrl: './ordem-compra-sucesso.component.html',
  styleUrls: ['./ordem-compra-sucesso.component.css']
})
export class OrdemCompraSucessoComponent implements OnInit {

  //Significa pode ser decorado com um valor externo.
  //Posso fazer com que o componente pai onde é feito a instancia
  //Desse componente filho, e nessa instância que vou passar o idPedidoCompra
  @Input() public idPedidoCompra: number

  constructor(private _router: Router) { }

  ngOnInit() {
    console.log(this.idPedidoCompra)
    setTimeout(() => {
      this._router.navigate(['/conta'], {  state: { 'idPedido' : 'CompraRealizada' }, replaceUrl: true })
    }, 3000);
  }

}
