import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ParamsService } from 'src/app/services/params.service';
import { OfertasService } from 'src/app/ofertas.service';
import { Product } from 'src/app/interfaces/product';
import { Subscription } from 'rxjs';
import { OrdemCompraService } from 'src/app/ordem-compra.service';
import { AuthService } from 'src/app/auth.service';
import { Produto } from 'src/app/shared/produto.model';

@Component({
  selector: 'app-product-data',
  templateUrl: './product-data.component.html',
  styleUrls: ['./product-data.component.css']
})
export class ProductDataComponent implements OnInit {

  addProduct: FormGroup;
  private product : Product = {};
  private loading: any;
  private productId: string = '';
  private productSubscription: Subscription
  private productData: any
  private productClient: Array<any>
  private dadosUsuario: Array<any> = []
  private status: string = 'Ativo'
  private nomeTitulo: string
  private nomeDescricao: string
  private nomeValor: string
  public nomeAnunciante: string
  public nomeIDOferta: string


  public formulario: FormGroup = new FormGroup({
    'categoria': new FormControl(null, [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
    'titulo': new FormControl(null),
    'descricao_oferta': new FormControl(null, [Validators.required]),
    'anunciante': new FormControl(null, [Validators.required]),
    'valor': new FormControl(null, [Validators.required]),
    'destaque': new FormControl(null, [Validators.required]),
    'imagem1': new FormControl(null, [Validators.required]),
    'imagem2': new FormControl(null, [Validators.required]),
    'imagem3': new FormControl(null, [Validators.required]),
  })

  public formularioUpdate: FormGroup = new FormGroup({
    'categoria': new FormControl(null, [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
    'titulo': new FormControl(null),
    'descricao_oferta': new FormControl(null, [Validators.required]),
    'status': new FormControl(null, [Validators.required]),
    'valor': new FormControl(null, [Validators.required]),
    'destaque': new FormControl(null, [Validators.required]),
    'imagem1': new FormControl(null, [Validators.required]),
    'imagem2': new FormControl(null, [Validators.required]),
    'imagem3': new FormControl(null, [Validators.required]),
  })

  constructor(
    private _formBuilder: FormBuilder,
    private authService: AuthService,
    private activedRouter: ActivatedRoute,
    private ofertasService: OfertasService,
    private ordemCompraService: OrdemCompraService,
    private paramService: ParamsService
  ) {
    this.dadosUsuario = this.paramService.getUser()
   }

  ngOnInit() {
    this.produtosAdmin()
  }

  produtosAdmin() {
    this.productClient = []
    this.ofertasService.todasOfertas().subscribe((data) => {
      console.log('todos ofertas', data)
      this.productClient = data;
    })
    // this.ofertasService.getCarrinhosPorAdmin().subscribe((data) => {
    //   console.log('DADOS DO CARRINHO GERAL:', data)
    //   this.productClient = data
    // })
  }

  cadastrarProduto() {
    console.log(this.formulario)
    if (this.formulario.status === "INVALID") {
      console.log('Formulário está inválido')
      this.formulario.get('categoria').markAsTouched()
      this.formulario.get('titulo').markAsTouched()
      this.formulario.get('descricao_oferta').markAsTouched()
      this.formulario.get('status').markAsTouched()
      this.formulario.get('valor').markAsTouched()
      this.formulario.get('destaque').markAsTouched()
      this.formulario.get('imagem1').markAsTouched()
      this.formulario.get('imagem2').markAsTouched()
      this.formulario.get('imagem3').markAsTouched()
    } else {

      var data = [{
          "url": this.formulario.value.imagem1
       },
       {
         "url": this.formulario.value.imagem2
        },
        {
          "url": this.formulario.value.imagem3
        }

      ];


      console.log('entrou no else')
      let oferta: Produto = new Produto(
        this.formulario.value.categoria,
        this.formulario.value.titulo,
        this.formulario.value.descricao_oferta,
        this.formulario.value.anunciante,
        this.formulario.value.valor,
        this.formulario.value.destaque,
        data,
        this.status,
        new Date().toLocaleString()
      )

      this.ordemCompraService.cadastrarProduto(oferta).subscribe((data) => {
        console.log(data)
        this.produtosAdmin()
      })
    }
  }

  atualizarOferta() {
    console.log(this.formularioUpdate)

    if (this.formularioUpdate.status === "INVALID") {
      console.log('Formulário está inválido')
      this.formulario.get('categoria').markAsTouched()
      this.formulario.get('titulo').markAsTouched()
      this.formulario.get('descricao_oferta').markAsTouched()
      this.formulario.get('anunciante').markAsTouched()
      this.formulario.get('valor').markAsTouched()
      this.formulario.get('destaque').markAsTouched()
      this.formulario.get('imagens').markAsTouched()
    } else {

      var data = [{
          "url": this.formulario.value.imagem1
       },
       {
         "url": this.formulario.value.imagem2
        },
        {
          "url": this.formulario.value.imagem3
        }

      ];


      console.log('entrou no else')
      let oferta: Produto = new Produto(
        this.formularioUpdate.value.categoria,
        this.formularioUpdate.value.titulo,
        this.formularioUpdate.value.descricao_oferta,
        this.nomeAnunciante,
        this.formularioUpdate.value.valor,
        this.formularioUpdate.value.destaque,
        data,
        this.formularioUpdate.value.status,
        new Date().toLocaleString()
      )

      if(this.nomeIDOferta) {
        this.ordemCompraService.updateProduct(this.nomeIDOferta, oferta).subscribe((data_product) => {
          console.log('Dados do produto: ',data_product)
          this.produtosAdmin()
          window.location.reload()
        });
      }
      
    }
  }

  recebendoOferta(item) {
    console.log(item)
    // this.formularioUpdate.value.categoria = item.categoria
    this.nomeTitulo = item.titulo
    this.nomeDescricao = item.descricao_oferta
    this.nomeValor = item.valor
    this.nomeAnunciante = item.anunciante ? '' : 'Anunciante'
    this.nomeIDOferta = item._id

    
  }
  

}
