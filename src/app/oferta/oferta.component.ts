import { Component, OnInit, OnDestroy } from '@angular/core';
//Importando uma interface do Roteamento Angular
import { ActivatedRoute, Params } from '@angular/router';
import { OfertasService } from '../ofertas.service';
// import { Observable, interval, Observer, Subscription } from 'rxjs';
import { CarrinhoService } from '../carrinho.service';
import { Oferta } from '../shared/Oferta.model';



@Component({
  selector: 'app-oferta',
  templateUrl: './oferta.component.html',
  styleUrls: ['./oferta.component.css'],
  providers: [ OfertasService ]
})
export class OfertaComponent implements OnInit, OnDestroy {

  //Atributo do tipo Oferta
  public oferta: Oferta
  public snackBar: boolean = false

  //Atributos que vão receber o subscribe.
  // private tempoObservableSubscription: Subscription

  // //Mais um atributo para o subscription do segundo observable
  // //Que é o observableTeste
  // private meuObservableTesteSubscription: Subscription

  //Atributo para referênciar essa interface
  //Na construsção do componente, essa interface vai servir
  //Para instânciar o nosso atributo
  //private route : ActivatedRoute 
  //Para ter acesso as rotas
  //No momento de construção precisamos avisar ao Angular
  //Que ele precisa passar essa informação para dentro do componente

 constructor(
   private route: ActivatedRoute, 
   private ofertasService: OfertasService,
   private carrinhoService: CarrinhoService
   ) { }

  ngOnInit() {

    console.log('Itens do carrinho OFERTACOMPONENTE: ',  this.carrinhoService.exibirItens())
    //foto do parâmetro passado para a rota
    //Esse 1 é o ID
    //Igual definir no app.routes.ts
    //Exemplo da url: localhost:3000/ofertas/1
    //Essas informações ficam gravadas dentro do atributo params do snap
    console.log('ID recuperado da rota: '+this.route.snapshot.params['id_pedido'])
    // console.log('subId recuperado da rota: '+this.route.snapshot.params['subId'])
    // Fazendo um subscribe nesse array de parâmetros
    //O metodo subscribe permite encaminhar uma função de callback
    //Algo que será feito quando a rotas forem modificadas
    //Quando o parâmetro é modificado, podemos reagir de alguma forma

    // this.route.params.subscribe((parametro: any) => {
    //   console.log(parametro.id)
    // })
    //Recuperando a oferta com base no ID que estamos recuperando da rota
    //Com o snapshot
    //Snapshot é uma foto, ele copia os parâmetros contido na rota
    //No momento em que a rota for acessada.
    //Quando os parâmetros forem modificados, que é o nosso caso
    //Ou seja nós estamos na rota, e partir de interações com 
    //nossos componentes nós estamos modificando o parãmetros
    //Nessa situação o snapshot não vai recuperar esse valor atualizado

    //Então vou fazer um subscribe nos parâmetros da rota
    //Preciso recuperar o actived route
    //Com o params que retorna um observable
    //Então podemos dar um subscribe nesse params(OBSERVABLE)
    //informando para ele como ele pode tratar o disparo
    //De eventos em sua STREAM
    //O observable de params dispara um evento contendo os
    //parãmetros sempre quando existe uma alteração desses parâmetros
    // na rota



    this.route.params.subscribe((parametros: Params) => {
      //Vou passar um random que é ação que vai ser feita
      //Para cada mofificação dos parãmetros
      //Sendo que nesse caso vou recuperar o id
      //Aqui bem dizer estou combinando um SUBSCRIBE
      //E um OBSERVABLE
      //Que a cada evento de moficação de parâmetors da rota
      //Vai executar uma promisse
      //E eu vou encaminhar para essa promisse
      //O ID atualizado da rota.
      
      //Sempre que houver alguma alteração vou recuperar o método
      this.ofertasService.getOfertaPorId(parametros.id_oferta)
      .then((oferta: Oferta) => {
        //Ação que eu vou tomar quando a promessa estiver resolvida
        // console.log(oferta)
        this.oferta = oferta
        //Verificando o que tem dentro do objeto
        // console.log(' OFERTA OFERTA' ,this.oferta)
      })
    })


      // // Recuperando aqui o router
      // // O params retorna um observable q contém um objeto
      // // Esse objeto contém uma chave que é uma string
      // // Contendo qualquer valor
      // // Trata-se justamente das combinações de parametros
      // // Que estamos encaminhando na rota
      // // chamo o subscribe e pego esse parâmetro
      // // E tomo alguma ação.

      // // Temos a inscrição com o subscribe que é um observador
      // // e estamos fazendo o observável deve lidar com instruções.
      // // Que é justamente a ação que devo fazer com o parâmetro recebido

      // // O 1º Parâmetro é o que ele deve fazer com instrução
      // // O 2º Parâmetro é o que ele deve fazer com erro
      // // O 3º Parâmetro é o que ele deve fazer quando estiver completo
      // // Ou seja concluido.
      // // this.route.params.subscribe(
      // //   (parametro: any) => { console.log(parametro) },
      // //   (error: any) => { console.log(error) },
      // //   () => console.log('Processamento foi classificado como concluído')
      // // )


      // let tempo = interval(2000)

      // //Esse subscribe eu quero indiciar para o observável
      // //Como que ele tem que lidar a cada disparado de evento
      // //Na STREAM.
      // //Recuperando o parâmetro fornecido 
      // // Criei uma variavel chamada intervalo do timpo number
      // //E ele vai receber esse inteiro disparado
      // //Em cada um dos eventos, a cada 500 milisegundos
      // //E ação que vou tomar é dar um console
      // this.tempoObservableSubscription = tempo.subscribe((intervalo: number) => {
      //   console.log(intervalo)
      // })

      // //De um lado eu preciso
      // //Dentro do create eu recebo os parâmetros
      // //Que estão vindo do subscribe
      // //Do observador.
      // //Observable (observável)
      // let meuObservableTeste = Observable.create((observer: Observer<string>) => {
      //   //Metodo next ele dispara um evento, 
      //   //no fluxo de processamento do Observável
      //   //E podemos falar por exemplo que esse metodo
      //   //Ele vai encaminhar uma string chamada primeiro evento

      //   //E quem diz como esse evento vai ser tratado é
      //   //Justamente o método que estamos encaminhando
      //   //O primeiro parâmetro encaminhado no SUBSCRIBE é a ação
      //   // que indica que como o que o nosso observável vai lidar
      //   //com os dados disparados a apartir dos eventos da STREAM.
      //   //Ou seja o subscribe vai receber por parâmetro o que eu colocar
      //   //No next
      //   observer.next('Primeiro evento da STREAM')
      //   observer.next('Segundo evento da STREAM')
      //   //Ecerrand o o observer
      //   // observer.error('Algum erro foi encontrado na STREAM de eventos')
      //   observer.complete()
      //   //Indicando ao observável como ele vai tratar isso
      //   //E eventos após o erro não são disparados após isso
      //   //Mesmo se eu colocar um next depois do error não é executado
      //   //O metodo complete ele executa com sucesso a stream de eventos
      //   //Porém
      // })

      // //Do outro lado
      // //Observable (observador)
      // //Dentro do subscribe eu passo uma instrução para o create receber
      // //Vou atribuir essa referência abaixo aos atributos que criei
      // //Atribuindo a variavel a esses resultados
      // //Eu crio uma referência para eles
      // //E ai eu posso importar o onDestroy
      // this.meuObservableTesteSubscription = meuObservableTeste.subscribe(
      //   (resultado: any) => console.log(resultado),
      //   (erro: any) => console.log(erro),
      //   () => console.log('STREAM de eventos foi finalizada')
      // )

      //Finalizando o subscribe tem 3 parâmetros que encaminhamos funções
        //Que recuperam os parâmetros produzidos em cada um dos eventos
        //Disparado nas stream. E essas funções elas estruturam como
        //As nossas informações devem ser tratadas.
        //No 1º Parâmetro vai instrução de como lhe dar com a produção
        //De algum tipo de informação q é o next
        //No 2º Parâmetro vai a instrução de como tratar o erro
        //No 3º Parâmetro como tratar a finalização
        //Da stream de dados.

        //Se pararmos para analisar ele se parece muito com as promisse
        //Onde temos o then e o catch, a diferença nesse caso
        //É que ganhamos o complete e vários operadores do rxjs 
        //Que ajuda nas nossas aplicações.

  }

  public adicionarItemCarrinho(): void {
    this.snackBar = true
    // Passando a oferta no template e recebendo aqui
    // console.log(this.oferta)
    //Vou pegar esse objeto e passar para o carrinhoService como sendo um item do pedido
    this.carrinhoService.incluirItem(this.oferta)
    setTimeout(() => {
      this.snackBar = false
    }, 1000);
    
    console.log(this.carrinhoService.exibirItens())
  }

  ngOnDestroy() {
    // Chamado uma vez, antes que a instância seja destruída.
    // Adicione 'implementa OnDestroy' à classe.
    // this.meuObservableTesteSubscription.unsubscribe()
    // this.tempoObservableSubscription.unsubscribe()
    
  }

}
