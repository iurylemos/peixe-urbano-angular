import { Cliente } from './shared/cliente.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URL_API } from './app.api';
import { Observable, BehaviorSubject } from 'rxjs';
import { map, retry } from 'rxjs/operators';
import { FormBuilder } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection, DocumentData } from '@angular/fire/firestore';
import { User } from './interfaces/user';

@Injectable({ providedIn: 'root' })
export class AuthService {
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;
  public userCliente: any
  private userCollection: AngularFirestoreCollection<User>;
  private userFire : User = {}

  constructor(
    private http: HttpClient,
    private fireAuth: AngularFireAuth,
    private fireBase: AngularFirestore,

  ) {
    this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
    this.userCollection = this.fireBase.collection<User>('Users')
  }

  public get currentUserValue(): any {
    this.currentUserSubject.subscribe((data) => {
      console.log('DADOS: ', data)
    })
    console.log(this.currentUserSubject.next)
    console.log(this.currentUserSubject.getValue())
    console.log(this.currentUserSubject.value)


    return this.currentUserSubject.value;
  }

  public login(email: any, password: any) {
    console.log('email', email)
    console.log('senha', password)
    console.log('entrou aqui no LOGIN do AUTH SERVICE')
    console.log(this.userCliente)

    try {
      return this.fireAuth.auth.signInWithEmailAndPassword(email, password).then((data) => {
        this.userCliente = data
        console.log(data)
        localStorage.setItem('currentUser', JSON.stringify(this.userCliente));
        this.currentUserSubject.next(this.userCliente)
        window.location.reload()
        return data
      });

    } catch (error) {
      console.log(error)
    }
  }

  public logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    localStorage.clear()
    // this.currentUserSubject = new BehaviorSubject<Cliente>(null);
    this.currentUserSubject.next(null);
    this.fireAuth.auth.signOut();
    window.location.reload()
  }

  public register(usuarioRegister: User, email: any, password: any) {

    console.log(usuarioRegister)
    
    try {
      return this.fireAuth.auth.createUserWithEmailAndPassword(email, password).then((data) => {
        this.userCliente = data
        console.log(data)
        localStorage.setItem('currentUser', JSON.stringify(this.userCliente));
        this.currentUserSubject.next(this.userCliente)
        this.userCollection.add(usuarioRegister)
        window.location.reload()
        return data
      });

    } catch (error) {
      console.log(error)
    }
  }

  public async getUser(email: string) {
    const snapchot = await this.userCollection.ref.get();
    return new Promise<DocumentData[]>(resolve => {
      const v = snapchot.docs.map(x => {
        const obj = x.data();

        return obj
      });
      const filter = v.filter(data => data.email === email)
      resolve(filter);
    });
  }

  public getUsers() {
    return this.userCollection.snapshotChanges().pipe(
      //Pipe para pecorrer as informações do snapshot
      map(actions => {
        return actions.map( a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;

          return { id, ...data };
        });
      })
    );
}


}