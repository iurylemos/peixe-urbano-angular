import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ParamsService } from 'src/app/services/params.service';
import { OfertasService } from 'src/app/ofertas.service';
import { OrdemCompraService } from 'src/app/ordem-compra.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UpdatePedido } from 'src/app/shared/updatePedido.model';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {

  private productDatas: Array<any> = []
  private productData: any = {}
  private exibirProduto: Array<any> = []
  private dadosUsuario: Array<any> = []
  private idPedido: any = {}
  public userAdmin: boolean

  public formularioModal: FormGroup = new FormGroup({
    'status': new FormControl(null, [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
    'formaPagamento': new FormControl(null),
    'endereco': new FormControl(null, [Validators.required]),
  })

  constructor(
    private activedRouter: ActivatedRoute,
    private paramService: ParamsService,
    private ofertasService: OfertasService,
    private ordemCompraService: OrdemCompraService,
  ) { 
    // this.router()
    this.dadosUsuario = this.paramService.getUser()
    if(this.dadosUsuario) {
      for (let index = 0; index < this.dadosUsuario.length; index++) {
        const element = this.dadosUsuario[index];
        this.userAdmin = element.isAdmin   
      }
      console.log('USUÁRIO ADMIN? ', this.userAdmin)
    }
    
  }

  ngOnInit() {
    this.router()
  }

  async router() {
    this.productData = this.activedRouter.snapshot.params;
    console.log('Entrou no dados do produto: ', this.productData)
    this.productDatas = this.paramService.getCarrinhoCliente()

    console.log("CARRINHO:", this.productDatas)

    if (this.productDatas !== undefined) {
      this.exibirProduto = this.productDatas.filter((data) => data._id === this.productData.id)
    } else {
      this.ofertasService.getCarrinhosPorAdmin().subscribe((data) => {
        console.log('DADOS DO CARRINHO ADMIN:', data)

        this.productDatas = data
        this.exibirProduto = this.productDatas.filter((data) => data._id === this.productData.id)
        for (let index = 0; index < this.exibirProduto.length; index++) {
          const element = this.exibirProduto[index];
          this.idPedido = element._id
        }

        console.log('ID PEDIDO',this.idPedido)
      })
    }

    console.log(this.exibirProduto)

    console.log('DADOS DO CARRINHO: ', this.productDatas)
  }

  atualizarProduto() {
    console.log(this.formularioModal)
    if (this.formularioModal.status === "INVALID") {
      console.log('Formulário está inválido')
      this.formularioModal.get('status').markAsTouched()
      this.formularioModal.get('formaPagamento').markAsTouched()
      this.formularioModal.get('endereco').markAsTouched()
    }else {
      let pedidos: UpdatePedido = new UpdatePedido(
        this.formularioModal.value.status,
        this.formularioModal.value.formaPagamento,
        this.formularioModal.value.endereco
      )
      console.log(pedidos)
      if (this.idPedido) {
        this.ordemCompraService.updatePedido(this.idPedido, pedidos).subscribe((data_pedido) => {
        console.log('Dados do pedido: ', data_pedido)
          this.router()
          window.location.reload()
        });
      }
    }
  }

}
