import { Component, Output, Input } from '@angular/core';
import { Cliente } from './shared/cliente.model';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  exibirLoginText: boolean = false
  email: string
  title = 'passaro-urbano';
  @Input() respostaDataUser: any

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    console.log('Entrou')
    // console.log(this.exibirLogin)
  }

  reciverFeedback(respostaFilho) {
    this.exibirLoginText = true
    console.log('Foi emitido o evento e chegou no pai >>>> ', respostaFilho);
  }

  openLayout(event) {
    console.log(event)
    // console.log(event[0].email)
    this.respostaDataUser = event
    this.exibirLoginText = false
  }
}
