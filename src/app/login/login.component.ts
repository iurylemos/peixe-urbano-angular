import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AlertService } from '../services/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [ AuthService ]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  openRegister: boolean = false;
  loading = false;
  submitted = false;
  returnUrl: string;
  @Output() customer: EventEmitter<any> = new EventEmitter<any>()
  /**
   * Constructor
   *
   * @param {FuseConfigService} _fuseConfigService
   * @param {FormBuilder} _formBuilder
   */
  constructor(
    private _formBuilder: FormBuilder,
    private authenticationService: AuthService,
    private alertService: AlertService,
    private router: Router,
  ) {
    
  }

  ngOnInit() {
    this.loginForm = this._formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
    console.log(this.loginForm.value)
  }

  abrirCadastro() {
    this.openRegister = true
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    // reset alerts on submit
    // this.alertService.clear();

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    console.log(this.loginForm)
    console.log(this.f)

    this.loading = true;
    this.authenticationService.login(this.f.email.value, this.f.password.value).then((data) => {
      console.log(data)
          this.customer.emit(data[0])
          this.router.navigate([''])
    }, (error) => {
      this.alertService.error(error);
          console.log(error)
          this.loading = false;
    })
  }

  openLogin() {
    this.openRegister = false
  }

  openHome() {
    window.location.reload()
  }

}
