import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { OfertasService } from '../ofertas.service';
import { Observable, Subject, of } from 'rxjs';
import { Oferta } from '../shared/Oferta.model';
import { switchMap, debounceTime, distinctUntilChanged, catchError } from 'rxjs/operators';
import { AuthService } from '../auth.service';
import { Cliente } from '../shared/cliente.model';


@Component({
  selector: 'app-topo',
  templateUrl: './topo.component.html',
  styleUrls: ['./topo.component.css'],
  providers: [ OfertasService, AuthService ]
})
export class TopoComponent implements OnInit {

  //Como o termo da pesquisa é string, coloquei do tipo String
  private subjectPesquisa: Subject<string> = new Subject<string>()
  private exibirLogin: boolean = true
  private botaoLogin: boolean = true
  @Input() recebeFamilia;
  @Output() respostaFamilia = new EventEmitter();
  currentUser: Cliente;

  //Atributo do tipo Observable para receber a resposta do pesquisa
  public ofertas: Observable<Oferta[]>

  constructor(
    private ofertasService: OfertasService,
    private authenticationService: AuthService  
  ) { 
    this.authenticationService.currentUser.subscribe((x) => {
      console.log(x)
      this.currentUser = x
    });
  }

  ngOnInit() {
    this.ofertas = this.subjectPesquisa //retorno Oferta[]
      .pipe(debounceTime(1000), //Executa ação do switchMap após 1 segundo
      distinctUntilChanged(), //Fazer pesquisas distintas
      switchMap((termo: string) => {
        console.log('Requisição HTTP para API')
        //Validação para campo que está vázio
        if(termo.trim() === '') {
          //Retornar um observable de array de ofertas vázio.
          //Ou seja não vai nem realizar a pesquisa
          //Montamos um observable e devolve para quem fez a chamda
          //Ou melhor.. O subscribe dentro do observable
          //Com o of eu consigo dizer qual que é o tipo
          //Desse observable.
          return of<Oferta[]>([])
        }
        //Retornando um Observable array de ofertas
        return this.ofertasService.pesquisaOfertas(termo)
      })
      ).pipe(catchError((erro: any) => {
        console.log(erro)
        //Retorno do que o subscribe espera do retorno
        return of<Oferta[]>([])
      }))

      //Como que eu sei que é o array de ofertas q está sendo retornado?
      //Simples: o método pesquisaOfertas ele retorna um observable
      //Cujo o seu conteudo é o array de ofertas extraido
      //Dentro do body da request.

      //Acessando o this.ofertas q contem o observable
      //Retornado pelo subject para que eu possa
      //A apartir do subscribe indicar como que esse evento
      //Ele tem que tratar o retorno produzido na STREAM.
      // this.ofertas.subscribe((ofertas: Oferta[]) => {
        //Vou atribuir a ela, e pecorre-la no template
        // console.log(ofertas)
        // this.ofertas2 = ofertas
      // })
      console.log(this.recebeFamilia)
      console.log(this.currentUser)
      if(this.currentUser === null) {
        this.botaoLogin = true
      }else {
        this.botaoLogin = false
      }
  }

  //Recuperando evento de texto dentro do input
  //Fazendo com que os dados do evento, sejam enviandos para o componente
  //Extraindo o value do campo input

  public fazerLogout() {
    this.authenticationService.logout()
    this.botaoLogin = true
  }

  public pesquisa(termoDaBusca: string): void {
    //A partir do evento, eu preciso recuperar o atributo target.value
    //Sendo que o evento corresponde a um objeto
    //Que representa estado do input no dom
    //No momento em que o evento ocorreu.
    //E o target é um desses atributo
    //Dessa forma conseguimos recuperar a instrução
    //Continda no value dele.
    // console.log((<HTMLInputElement>event.target).value)
    // console.log(termoDaBusca)
    // this.ofertas = this.ofertasService.pesquisaOfertas(termoDaBusca)
    // console.log(this.ofertas)
    // this.ofertas.subscribe(
    //   (ofertas: Oferta[]) => console.log(ofertas),
    //   (erro: any) => console.log('Erro status: ', erro),
    //   () => console.log('Fluxo de eventos completo! ')
    // )
    
    //Utilizando o metodo next
    //Sempre quando houver um novo termo de busca
    //Eu vou enviar esse termo de buscar
    //Para dentro do subject
    //E isso é o subject do lado Observador
    this.subjectPesquisa.next(termoDaBusca)
    console.log('key up caracter: ', termoDaBusca)
  }

  public limpaPesquisa(): void {
    //Encaminhar um novo valor para o observable.
    //No momento em que o PROXY(SUBJECTPESQUISA)
    //Receber um valor vázio
    //Ele vai entrar dentro do if que eu criei
    //Q está dentro do switchMap
    //Retornando um observable contendo um array vázio.
    //Automaticamente o ng for não vai ter nada para renderizar
    //Então dessa forma consigo limpar de forma mais rápida
    this.subjectPesquisa.next('')
  }

  public abrirLogin() : void {
    this.exibirLogin = false
    this.respostaFamilia.emit('Abrir Login')
  }

}
