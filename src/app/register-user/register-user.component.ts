import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AuthService } from '../auth.service';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { User } from '../interfaces/user';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  private userFire : User = {}
  @Output() respostaFamilia = new EventEmitter();

  constructor(
      private formBuilder: FormBuilder,
      private authenticationService: AuthService,
      private router: Router,
      // private userService: UserService,
      // private alertService: AlertService
  ) {
      // redirect to home if already logged in
      // if (this.authenticationService.currentUserValue) {
      //     this.router.navigate(['/']);
      // }
  }

  ngOnInit() {
      this.registerForm = this.formBuilder.group({
          nome: ['', Validators.required],
          email: ['', Validators.required],
          password: ['', [Validators.required, Validators.minLength(6)]]
      });
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
      this.submitted = true;
      this.userFire.isAdmin = false

      console.log(this.registerForm)
      console.log(this.f)
      console.log(this.userFire)
      // reset alerts on submit
      // this.alertService.clear();

      // stop here if form is invalid
      if (this.registerForm.invalid) {
          return;
      }

    this.loading = true;
    this.authenticationService.register(this.userFire,this.f.email.value, this.f.password.value).then((data) => {
        console.log(data) 
        this.router.navigate([''])
    }, (error) => {
        console.log(error)
    })
  }

  voltarLogin() {
      console.log('Entrou aqui')
      this.respostaFamilia.emit("ExibirLogin")
  }
}