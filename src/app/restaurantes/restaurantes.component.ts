import { Component, OnInit, NgZone } from '@angular/core';
//Recuperar o modelo de dados que é o Ofertas
import { Oferta } from '../shared/Oferta.model';
//Executar o metodo a partir da instância do serviço
import { OfertasService } from '../ofertas.service';


@Component({
  selector: 'app-restaurantes',
  templateUrl: './restaurantes.component.html',
  styleUrls: ['./restaurantes.component.css'],
  providers: [ OfertasService ]
})
export class RestaurantesComponent implements OnInit {

  public ofertas : Oferta[]
  public filterCategory: string

  //Atributo data
  // public dataTeste: any = new Date(2019, 8, 30)

  constructor(
    private ofertasService: OfertasService,
    private zone: NgZone
    
  ) { }

  ngOnInit() {
    //Esse metodo retorna uma promisse
    this.ofertasService.getOfertasPorCategoria('mercearia')
    //Then para a resposta, passando o arrowfunction
    //Que é ação que eu vou tomar, quando a resposta estiver pronta
      .then(( ofertas: Oferta[] ) => {
        this.ofertas = ofertas
        console.log('COMPONENTE RESTAURANTES',ofertas[0])
      })
  }

  //Recuperar o modelo de dados que é o Ofertas

  async open(event) {
    this.ofertas = []
    console.log('Entrou no OPEN', event)
    this.filterCategory = event.target.value
    // console.log(this.filterCategory)
    this.zone.run(async () => {
      await this.ofertasService.getOfertasPorCategoria(this.filterCategory)
        .then((oferta: Oferta[]) => {
          this.ofertas = oferta
        }).catch((error) => {
          console.log('error', error)
        })
    })
  }

}
