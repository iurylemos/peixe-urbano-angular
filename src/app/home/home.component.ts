import { Component, OnInit, NgZone } from '@angular/core';
import { OfertasService } from '../ofertas.service';
import { Oferta } from '../shared/Oferta.model';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';
import { Cliente } from '../shared/cliente.model';
import { ParamsService } from '../services/params.service';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [OfertasService, AuthService]
})
export class HomeComponent implements OnInit {

  //Atributo oferta
  public ofertas: Oferta[]
  currentUser: Cliente;
  returnUrl: string;
  private produtosCadastrados: boolean = false;
  private produtosAdmin: Array<any> = []
  public dadosUser: Array<any> = []
  public carrinhoClient: Array<any> = []
  public filterPedidos: Array<any> = []
  public userData: string
  public userAdmin: boolean
  loading: boolean = false

  constructor(
    private ofertasService: OfertasService,
    private router: ActivatedRoute,
    private authenticationService: AuthService,
    private paramService: ParamsService,
    private zone: NgZone,
    private fireAuth: AngularFireAuth,
    ) {
      this.authenticationService.currentUser.subscribe((x) => {
        console.log(x)
        this.currentUser = x
      });
     }

  ngOnInit() {

    this.router.queryParams.subscribe((result) => {
      console.log('RESULTADO RECEBIDO DO ORDEM-COMPRA', result)
    })

    //Recebido do ordem-compra quando o usuário não está logado
    // this.returnUrl = this.router.snapshot.queryParams['returnUrl']
    // console.log('Recebido do ordem-compra quando o usuário não está logado')
    // console.log('returnURL',this.returnUrl)
    

    this.ofertasService.getOfertas()
      .then(( ofertas: Oferta[] ) => {
        console.log('A função resolve() foi resolvida depois de 3 segundos')
        console.log('COMPONENTE HOME', ofertas)
        //Resolve da promisse
        this.ofertas = ofertas
        this.loading = true

      })
      .catch((param: any) => {
        //Reject da promisse
        console.log(param)
      })
  }

}
